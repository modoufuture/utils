<?php

namespace modoufuture\utils;

use modoufuture\utils\traits\arr\Is;
use modoufuture\utils\traits\arr\Items;
use modoufuture\utils\traits\arr\Split;

/**
 * Class Arr
 * @package modoufuture\utils
 */
class Arr
{
    /**
     * @var int 普通统计
     */
    const COUNT_NORMAL = 0;
    /**
     * @var int 递归统计
     */
    const COUNT_RECURSIVE = 1;
    /**
     * @var int 递归统计，不含自身
     */
    const COUNT_RECURSIVE_WITHOUT_SELF = 2;

    use Is;
    use Items;
    use Split;
}