<?php

namespace modoufuture\utils;

use modoufuture\utils\traits\html\Attributes;
use modoufuture\utils\traits\html\Elements;
use modoufuture\utils\traits\html\Tags;

class Html
{
    use Elements;
    use Attributes;
    use Tags;
}