<?php

namespace modoufuture\utils;

use Jenssegers\Agent\Agent;
use Zhuzhichao\IpLocationZh\Ip;

class Client
{
    /**
     * @var array $ipCache ip缓存
     */
    protected static $ipCache = [];
    /**
     * @var null|Agent $agent Agent实例
     */
    protected static $agent = null;

    /**
     * 获取用户ua
     * @return string
     */
    public static function ua()
    {
        return self::getAgent()->getUserAgent()?:"Unknown";
    }

    /**
     * 获取操作系统
     * @param null|string $userAgent 用户的user-agent
     * @return string
     */
    public static function os($userAgent=null)
    {
        return self::getAgent()->platform($userAgent);
    }

    /**
     * 判断是否是移动设备访问， 不区分是手机还是平板
     * @param null|string $userAgent 用户的user-agent
     * @return bool
     */
    public static function isPhone($userAgent=null)
    {
        return self::getAgent()->isPhone($userAgent);
    }

    /**
     * 判断是否是手机设备访问
     * @param null|string $userAgent 用户的user-agent
     * @return bool
     */
    public static function isMobile($userAgent=null)
    {
        return self::getAgent()->isMobile($userAgent);
    }

    /**
     * 获取用户设备信息
     * @param null|string $userAgent 用户的user-agent
     * @return string
     */
    public static function device($userAgent=null)
    {
        return self::getAgent()->device($userAgent);
    }

    /**
     * 获取客户端ip
     * @param int $type 返回类型 0 返回IP地址 1 返回IPV4地址数字
     * @return mixed
     */
    public static function ip($type = 0)
    {
        $type      = $type ? 1 : 0;
        $server = $_SERVER;
        $ip = 'unknown';
        if (!empty($server['HTTP_X_FORWARDED_FOR'])) {
            $arr = explode(',', $server['HTTP_X_FORWARDED_FOR']);
            $pos = array_search('unknown', $arr);
            if (false !== $pos) {
                unset($arr[$pos]);
            }
            $ip = trim(current($arr));
        } else {
            $keys = ['HTTP_X_REAL_IP','HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED', 'HTTP_FORWARDED', 'REMOTE_ADDR'];
            foreach ($keys as $key) {
                $ip = getenv($key);
                if ($ip) {
                    break;
                }
            }
        }

        // IP地址类型
        $ip_mode = (strpos($ip, ':') === false) ? 'ipv4' : 'ipv6';
        // IP地址合法验证
        if (filter_var($ip, FILTER_VALIDATE_IP) !== $ip) {
            $ip = ('ipv4' === $ip_mode) ? '0.0.0.0' : '::';
        }
        // 如果是ipv4地址，则直接使用ip2long返回int类型ip；如果是ipv6地址，暂时不支持，直接返回0
        $long_ip = ('ipv4' === $ip_mode) ? sprintf("%u", ip2long($ip)) : 0;

        $ip = [$ip, $long_ip];

        return $ip[$type];
    }

    /**
     * 获取地理位置
     * @param null $ip
     * @param array $options 配置信息
     *                          [
     *                              'string' => true/false,   返回为字符串
     *                              'code'  => true/false,    是否带有邮编
     *                              'format' => '%s, $s, $s'    字符串格式化
     *                          ]
     * @return mixed|string
     */
    public static function location($ip = null, $options=[])
    {
        if (!$ip) {
            $ip = self::ip();
        }
        $location = Ip::find($ip);
        if (empty($options['code']) && is_array($location)) {
            array_pop($location);
        }
        if (!empty($location['string']) && is_array($location)) {
            $location = !empty($options['format']) ?
                call_user_func_array('sprintf', [$options['format']]+$location) :
                implode('', $location);
        }

        return $location;
    }

    /**
     * 获取浏览器信息
     * @param null|string $userAgent 用户的user-agent
     * @param bool $more 是否获取详情
     * @return string
     */
    public static function browser($userAgent=null, $more = false)
    {
        if ($more) {
            $info = [
                'browser' => self::getAgent()->browser($userAgent),
                'device' => self::getAgent()->device($userAgent),
            ];
            $info['version'] = self::getAgent()->version($info['browser']);

            return sprintf('%s (%s version %s)', $info['browser'], $info['device'], $info['version']);
        }

        return self::getAgent()->browser($userAgent);
    }

    /**
     * 获取agent
     *
     * @return Agent|null
     */
    public static function getAgent()
    {
        if (!self::$agent) {
            self::$agent = new Agent();
        }

        return self::$agent;
    }
}