<?php

namespace modoufuture\utils;
/**
 * Class Json
 * @package modoufuture\utils
 */
class Json
{
    /**
     * @var array $jsonErrorMessages 错误提示信息
     */
    public static $jsonErrorMessages = [
        'JSON_ERROR_DEPTH' => 'The maximum stack depth has been exceeded.',
        'JSON_ERROR_STATE_MISMATCH' => 'Invalid or malformed JSON.',
        'JSON_ERROR_CTRL_CHAR' => 'Control character error, possibly incorrectly encoded.',
        'JSON_ERROR_SYNTAX' => 'Syntax error.',
        'JSON_ERROR_UTF8' => 'Malformed UTF-8 characters, possibly incorrectly encoded.', // PHP 5.3.3
        'JSON_ERROR_RECURSION' => 'One or more recursive references in the value to be encoded.', // PHP 5.5.0
        'JSON_ERROR_INF_OR_NAN' => 'One or more NAN or INF values in the value to be encoded', // PHP 5.5.0
        'JSON_ERROR_UNSUPPORTED_TYPE' => 'A value of a type that cannot be encoded was given', // PHP 5.5.0
    ];

    /**
     * @param $data
     * @param int $options
     * @return false|string
     * @throws \Exception
     */
    public static function encode($data, $options = JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE)
    {
        $data = static::process($data);
        set_error_handler(function () {
            static::jsonErrorHandle(JSON_ERROR_SYNTAX);
        }, E_WARNING);
        $json = json_encode($data, $options);
        restore_error_handler();
        static::jsonErrorHandle(json_last_error());

        return $json;
    }


    /**
     * @param $json
     * @param bool $assoc
     * @return mixed|null
     * @throws \Exception
     */
    public static function decode($json, $assoc = true)
    {
        if (is_array($json)) {
            throw new \Exception('Invalid JSON data.');
        } elseif ($json === null || $json === '') {
            return null;
        }
        $decode = json_decode((string) $json, $assoc);
        static::jsonErrorHandle(json_last_error());

        return $decode;
    }


    /**
     * @param $data
     * @return false|string
     */
    public static function htmlEncode($data)
    {
        try {
            return static::encode($data, JSON_UNESCAPED_UNICODE | JSON_HEX_QUOT | JSON_HEX_TAG | JSON_HEX_AMP | JSON_HEX_APOS);
        } catch (\Exception $e) {

            return $e->getMessage();
        }
    }

    /**
     * @param $value
     * @param array $xmlOptions
     * @return mixed
     * @throws \Exception
     */
    public static function toXml($value, $xmlOptions = [])
    {
        $value = Arr::isTraversable($value) ? $value : static::decode($value, true);

        return Xml::encode($value, $xmlOptions);
    }

    /**
     * 处理数据
     * @param $data
     * @return array|\SimpleXMLElement|\stdClass
     */
    protected static function process($data)
    {
        if (is_object($data)) {
            if ($data instanceof \JsonSerializable) {
                return static::process($data->jsonSerialize());
            } elseif ($data instanceof \SimpleXMLElement) {
                $data = (array) $data;
            } else {
                $result = [];
                foreach ($data as $name => $value) {
                    $result[$name] = $value;
                }
                $data = $result;
            }
            if ($data === []) {
                return new \stdClass();
            }
        }
        if (is_array($data)) {
            foreach ($data as $key => $value) {
                if (is_array($value) || is_object($value)) {
                    $data[$key] = static::process($value);
                }
            }
        }
        return $data;
    }

    /**
     * 接管错误处理
     * @param int $lastError 错误号
     * @return void
     * @throws \Exception
     */
    protected static function jsonErrorHandle($lastError)
    {
        if ($lastError === JSON_ERROR_NONE) {
            return;
        }
        $errors = [];
        foreach (static::$jsonErrorMessages as $const => $message) {
            if (defined($const)) {
                $errors[constant($const)] = $message;
            }
        }
        if (isset($errors[$lastError])) {
            throw new \Exception($errors[$lastError], $lastError);
        }
        throw new \Exception('Unknown JSON encoding/decoding error.');
    }
}