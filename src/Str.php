<?php

namespace modoufuture\utils;

class Str
{
    public static $encoding = 'UTF-8';

    /**
     * @param $str
     * @param $length
     * @param string $suffix
     * @param string $encoding
     * @param bool $asHtml
     * @return string
     * @throws \HTMLPurifier_Exception
     */
    public static function cut($str, $length, $suffix = '...', $encoding = 'UTF-8', $asHtml = false)
    {
        if ($asHtml) {
            return static::cutHtml($str, $length, $suffix, $encoding);
        }

        if (mb_strlen($str, $encoding) > $length) {
            return rtrim(mb_substr($str, 0, $length, $encoding)) . $suffix;
        }

        return $str;
    }

    /**
     * @param $str
     * @param $count
     * @param string $suffix
     * @param bool $asHtml
     * @return string
     * @throws \HTMLPurifier_Exception
     */
    public static function cutStr($str, $count, $suffix = '...', $asHtml = false)
    {
        if ($asHtml) {
            return static::cutHtml($str, $count, $suffix);
        }

        $words = preg_split('/(\s+)/u', trim($str), null, PREG_SPLIT_DELIM_CAPTURE);
        if (count($words) / 2 > $count) {
            return implode('', array_slice($words, 0, ($count * 2) - 1)) . $suffix;
        }

        return $str;
    }

    /**
     * @param $str
     * @param $count
     * @param $suffix
     * @param bool $encoding
     * @return string
     * @throws \HTMLPurifier_Exception
     */
    protected static function cutHtml($str, $count, $suffix, $encoding = false)
    {
        $config = \HTMLPurifier_Config::create(null);
        $lexer = \HTMLPurifier_Lexer::create($config);
        $tokens = $lexer->tokenizeHTML($str, $config, new \HTMLPurifier_Context());
        $openTokens = [];
        $totalCount = 0;
        $depth = 0;
        $truncated = [];
        foreach ($tokens as $token) {
            if ($token instanceof \HTMLPurifier_Token_Start) { //Tag begins
                $openTokens[$depth] = $token->name;
                $truncated[] = $token;
                ++$depth;
            } elseif ($token instanceof \HTMLPurifier_Token_Text && $totalCount <= $count) { //Text
                if (false === $encoding) {
                    preg_match('/^(\s*)/um', $token->data, $prefixSpace) ?: $prefixSpace = ['', ''];
                    $token->data = $prefixSpace[1] . self::cutStr(ltrim($token->data), $count - $totalCount, '');
                    $currentCount = self::countWords($token->data);
                } else {
                    $token->data = self::cut($token->data, $count - $totalCount, '', $encoding);
                    $currentCount = mb_strlen($token->data, $encoding);
                }
                $totalCount += $currentCount;
                $truncated[] = $token;
            } elseif ($token instanceof \HTMLPurifier_Token_End) { //Tag ends
                if ($token->name === $openTokens[$depth - 1]) {
                    --$depth;
                    unset($openTokens[$depth]);
                    $truncated[] = $token;
                }
            } elseif ($token instanceof \HTMLPurifier_Token_Empty) { //Self contained tags, i.e. <img/> etc.
                $truncated[] = $token;
            }
            if ($totalCount >= $count) {
                if (0 < count($openTokens)) {
                    krsort($openTokens);
                    foreach ($openTokens as $name) {
                        $truncated[] = new \HTMLPurifier_Token_End($name);
                    }
                }
                break;
            }
        }
        $context = new \HTMLPurifier_Context();
        $generator = new \HTMLPurifier_Generator($config, $context);

        return $generator->generateFromTokens($truncated) . ($totalCount >= $count ? $suffix : '');
    }

    /**
     * @param $str
     * @return int
     */
    public static function countWords($str)
    {
        return count(preg_split('/\s+/u', $str, null, PREG_SPLIT_NO_EMPTY));
    }

    /**
     * @param $str
     * @return int
     */
    public static function length($str)
    {
        return mb_strlen($str, static::$encoding);
    }

    /**
     * @param $str
     * @return int
     */
    public static function byte($str)
    {
        return mb_strlen($str, '8bit');
    }

    /**
     * @param $str
     * @return bool|false|mixed|string|string[]|null
     */
    public static function lower($str)
    {
        return mb_strtolower($str, static::$encoding);
    }

    /**
     * @param $str
     * @return bool|false|mixed|string|string[]|null
     */
    public static function upper($str)
    {
        return mb_strtoupper($str, static::$encoding);
    }

    /**
     * @param $class
     * @param bool $namespace
     * @param bool $trans
     * @return string
     */
    public static function className($class, $namespace = false, $trans = false)
    {
        if (is_object($class)) {
            $class = get_class($class);
        }
        $className = static::basename($class);
        if ($trans) {
            $className = $trans == 1 ? static::camelCase($className, true) : static::underlineCase($className);
        }

        if ($namespace) {
            $namespace = str_replace('/', '\\', static::dirname($class));

            return !$namespace ? $className : '\\'.ltrim($namespace,'\\').'\\'.$className;
        }

        return $className;
    }

    /**
     * @param $path
     * @param string $suffix
     * @return string
     */
    public static function basename($path, $suffix = '')
    {
        if (($len = mb_strlen($suffix)) > 0 && mb_substr($path, -$len) === $suffix) {
            $path = mb_substr($path, 0, -$len);
        }
        $path = rtrim(str_replace('\\', '/', $path), '/\\');
        if (($pos = mb_strrpos($path, '/')) !== false) {
            return mb_substr($path, $pos + 1);
        }

        return $path;
    }

    /**
     * @param $path
     * @return string
     */
    public static function dirname($path)
    {
        $pos = mb_strrpos(str_replace('\\', '/', $path), '/');
        if ($pos !== false) {
            return mb_substr($path, 0, $pos);
        }

        return '';
    }

    /**
     * @param $str
     * @param bool $ucfirst
     * @example
     * ```php
     * echo Str::camelCase('a_b_c'); // ABC
     * echo Str::camelCase('a_b_c', false); // aBC
     * ```
     * @return string
     */
    public static function camelCase($str, $ucfirst = true)
    {
        $name = preg_replace_callback('/_([a-zA-Z])/', function ($match) {
            return strtoupper($match[1]);
        }, $str);

        return $ucfirst ? ucfirst($name) : lcfirst($name);
    }

    /**
     * @param $str
     * @example
     * ```php
     * echo Str::underlineCase('ABc'); // a_bc
     * ```
     * @return string
     */
    public static function underlineCase($str)
    {
        return strtolower(trim(preg_replace("/[A-Z]/", "_\\0", $str), "_"));
    }

    /**
     * @param $str
     * @return bool|false|mixed|string
     */
    public static function getEncoding($str)
    {
        return mb_detect_encoding($str);
    }

    /**
     * 移除非当前编码的字符
     * @param $str
     * @return string
     */
    public static function fixEncoding($str)
    {
        return htmlspecialchars_decode(htmlspecialchars($str, ENT_NOQUOTES | ENT_IGNORE, static::$encoding), ENT_NOQUOTES);
    }

    /**
     * 检查是否有非当前编码的字符
     * @param $str
     * @return bool
     */
    public static function checkEncoding($str)
    {
        return $str === static::fixEncoding($str);
    }

    /**
     * Encodes special characters into HTML entities.
     * @param $str
     * @param bool $doubleEncode
     * @return string
     */
    public static function encode($str, $doubleEncode = true)
    {
        return htmlspecialchars($str, ENT_QUOTES | ENT_SUBSTITUTE, static::$encoding, $doubleEncode);
    }

    /**
     * Decodes special HTML entities back to the corresponding characters.
     *
     * @param $str
     * @return string
     */
    public static function decode($str)
    {
        return htmlspecialchars_decode($str, ENT_QUOTES);
    }

    /**
     * 编码转换
     * @param $str
     * @param string $to
     * @param bool $from
     * @return bool|false|string|string[]|null
     */
    public static function convertEncoding($str, $to = 'UTF-8', $from = false)
    {
        empty($to) && $to = static::$encoding;
        if (empty($from)) {
            $from = mb_detect_encoding($str);
        }

        return mb_convert_encoding($str, $to, $from);
    }
}