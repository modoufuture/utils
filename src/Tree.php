<?php

namespace modoufuture\utils;
/**
 * Class Tree
 *
 * @package stargaze\helpers
 */
class Tree
{
    /**
     * @var array 缓存数据
     */
    private static $cacheArray = [];
    /**
     * @var array 原始分类数据
     */
    protected $rawList = [];
    /**
     * @var string 主键字段
     */
    protected $pkField = 'id';
    /**
     * @var string 上级分类字段
     */
    protected $parentField = 'pid';
    /**
     * @var string 生成数据下级分类存放key
     */
    protected $childrenField = 'children';
    /**
     * @var bool 是否以扁平化数据输出
     */
    protected $flat = false;
    /**
     * @var array|mixed|string 显示字段
     */
    protected $showFields = ['*'];
    /**
     * @var string 名称
     */
    protected $name = 'name';
    /**
     * @var string 填充名称
     */
    protected $padName = 'pad_name';
    /**
     * @var int 当前级别
     */
    protected $level = 1;
    /**
     * @var string 当前分类路径
     */
    protected $path = '';
    /**
     * @var string 路径分隔符
     */
    protected $pathDelimiter = ',';
    /**
     * @var array 当前元素
     */
    protected $current = [];
    /**
     * @var array 返回数据
     */
    protected $returnArray = [];
    /**
     * @var string 错误信息
     */
    protected $error = '';
    /**
     * @var array 显示图标
     */
    protected $icon = ['|', '|---'];

    public function __construct($config = [])
    {
        !empty($config['pk']) && $this->setPkField($config['pk']);
        !empty($config['delimiter']) && $this->setPathDelimiter($config['delimiter']);
        !empty($config['flat']) && $this->setFlat($config['flat']);
        !empty($config['pid']) && $this->setParentField($config['pid']);
        !empty($config['children']) && $this->setChildrenField($config['children']);

        !empty($config['fields']) && $this->setShowFields($config['fields']);
        !empty($config['name']) && $this->setName($config['name']);
        !empty($config['pad_name']) && $this->setPadName($config['pad_name']);
    }

    // 获取树状

    /**
     * @param string $pkField
     */
    public function setPkField($pkField)
    {
        $this->pkField = $pkField;
    }

    /**
     * @param string $pathDelimiter
     */
    public function setPathDelimiter($pathDelimiter)
    {
        $this->pathDelimiter = $pathDelimiter;
    }

    /**
     * @param bool $flat
     */
    public function setFlat($flat)
    {
        $this->flat = $flat;
    }

    /**
     * @param string $parentField
     */
    public function setParentField($parentField)
    {
        $this->parentField = $parentField;
    }

    /**
     * @param string $childrenField
     */
    public function setChildrenField($childrenField)
    {
        $this->childrenField = $childrenField;
    }

    /**
     * @param array|mixed|string $showFields
     */
    public function setShowFields($showFields)
    {
        $showFields = is_array($showFields) ? $showFields : explode(',', $showFields);
        $showFields = array_merge($showFields, [$this->pkField, $this->parentField]);
        $this->showFields = $showFields;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @param string $padName
     */
    public function setPadName($padName)
    {
        $this->padName = $padName;
    }

    public function getTree($array = [], $pid = 0, $level = 1)
    {
        $this->setRawList($array);
        $this->returnArray = [];
        $path = $pid . $this->pathDelimiter;
        if ($this->flat) {
            // 获取扁平化的数据
            $this->returnArray = $this->getFlatTree($pid, $level, $path);
        } else {
            // 获取带children的数据
            $this->returnArray = $this->getSunTree($pid, $level, $path);
        }
        self::$cacheArray = [];

        return $this->returnArray;
    }

    /**
     * @param array $rawList
     */
    public function setRawList($rawList)
    {
        $this->rawList = $rawList;
    }

    /**
     * 获取扁平化的数据
     *
     * @param int    $pid
     * @param int    $level
     * @param string $path
     *
     * @return array
     */
    protected function getFlatTree($pid = 0, $level = 1, $path = '')
    {
        foreach ($this->rawList as $k => $v) {
            if ($v[$this->parentField] == $pid) {
                $v = $this->filterItem($v, $level, $path);
                self::$cacheArray[] = $v;
                $this->getFlatTree($v[$this->pkField], $level + 1, $v['path'] . $v['id'] . $this->pathDelimiter);
            }
        }

        return self::$cacheArray;
    }

    /**
     * @param $item
     * @param $level
     * @param $path
     *
     * @return mixed
     */
    private function filterItem($item, $level, $path)
    {
        if (!in_array('*', $this->showFields)) {
            foreach ($item as $k => $v) {
                if (!in_array($k, $this->showFields)) {
                    unset($item[$k]);
                }
            }
        }
        $pre = '';
        if ($level > 1) {
            $pre = $this->icon[0] . str_repeat('&nbsp;', ($level - 1) * 4);
        }
        $item[$this->padName] = $pre . $this->icon[1] . $item[$this->name];
        $item['level'] = $level;
        $item['path'] = $path;
        //$item[$this->childrenField] = [];

        return $item;
    }

    /**
     * 获取子孙树
     *
     * @param int    $pid
     * @param int    $level
     * @param string $path
     *
     * @return array
     */
    protected function getSunTree($pid = 0, $level = 1, $path = '')
    {
        $tree = [];
        $items = $this->getFlatTree($pid, $level, $path);
        $items = array_column($items, null, $this->pkField);
        foreach ($items as $k => $v) {
            if ($v[$this->parentField] == $pid) {
                if (!isset($items[$v[$this->pkField]][$this->childrenField])) {
                    $items[$v[$this->pkField]][$this->childrenField] = [];
                }
                $tree[] = &$items[$v[$this->pkField]];
            } else {
                $items[$v[$this->parentField]][$this->childrenField][] = &$items[$v[$this->pkField]];
            }
        }

        return $tree;
    }

    /**
     * 获取家谱树
     *
     * @param array $array 数组
     * @param int   $id    要查找的id
     */
    public function getFamilyTree($array, $id, $self = true)
    {
        $this->returnArray = [];
        try {
            $array = array_column($array, null, $this->pkField);
            $selfInfo = isset($array[$id]) ? $array[$id] : [];
            if (empty($selfInfo)) {
                throw new \Exception('数据不存在');
            }
            $pid = $selfInfo[$this->parentField];
            do {
                $flag = isset($array[$pid]);
                if ($flag) {
                    $this->returnArray[] = $array[$pid];
                }
            } while ($flag);
            $self && $this->returnArray[] = $self;
        } catch (\Exception $e) {

        }

        return $this->returnArray;
    }

    /**
     * @return string
     */
    public function getError()
    {
        return $this->error;
    }
}