<?php

namespace modoufuture\utils;
/**
 * Class Xml
 * @package modoufuture\utils
 */
class Xml
{
    /**
     * @var array $options
     */
    protected static $options = [
        'version' => '1.0',
        'encoding' => 'utf-8',
        'root' => 'root',
        'root_attr' => '',
        'item' => 'item',
        'item_attr' => 'id'
    ];

    public static function encode($arr, $options = [])
    {
        $options && static::setOptions($options);
        $attr = static::opt('root_attr');
        if (is_array($attr)) {
            $arr = [];
            foreach($attr as $k => $v) {
                $arr[] = "{$k}=\"{$v}\"";
            }
            $attr = implode(' ', $arr);
        }

        $attr = trim($attr);
        $attr = empty($attr) ? '' : " {$attr}";
        $xml  = '<?xml version="'.static::opt('version','1.0').'" encoding="'.static::opt('encoding', 'utf-8').'"?>';
        $xml .= '<'.static::opt('root').$attr.'>';
        $xml .= static::dataToXml((array)$arr);
        $xml .= '<'.static::opt('root').'>';

        return $xml;
    }

    /**
     * @param $xml
     * @param bool $asArray
     * @return mixed|\SimpleXMLElement|null
     */
    public function decode($xml, $asArray = true)
    {
        if (!is_string($xml)) {
            return null;
        }
        if (function_exists('libxml_disable_entity_loader')) {
            libxml_disable_entity_loader(true);
        }
        $object = simplexml_load_string($xml);
        if (!$asArray) {
            return $object;
        }

        return json_decode(json_encode($object), true);
    }

    /**
     * @param array $options
     */
    public static function setOptions($options)
    {
        self::$options = Arr::merge(static::$options, $options);
    }

    /**
     * @param $key
     * @param string $default
     * @return mixed|null
     */
    protected static function opt($key, $default='')
    {
        return Arr::get(static::$options, $key, $default);
    }

    /**
     * @param $data
     * @return string
     */
    protected static function dataToXml($data)
    {
        $xml = $attr = '';
        $itemAttr = static::opt('item_attr');
        foreach($data as $k=>$v) {
            if (is_numeric($k)) {
                $itemAttr && $attr = " {$itemAttr}=\"{$k}\"";
                $k = static::opt('item');
            }
            $xml .= "<{$k}{$attr}>";
            $xml .= (is_array($v) || is_object($v)) ? static::dataToXml($v) : $v;
            $xml .= "</{$k}>";
        }

        return $xml;
    }
}