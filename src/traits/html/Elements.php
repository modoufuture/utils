<?php

namespace modoufuture\utils\traits\html;

use modoufuture\utils\Str;

/**
 * Trait Elements
 * @package modoufuture\utils\traits\html
 */
trait Elements
{
    /**
     * @var array $voidElements 单标签
     */
    protected static $voidElements = [
        'area' => 1,
        'base' => 1,
        'br' => 1,
        'col' => 1,
        'command' => 1,
        'embed' => 1,
        'hr' => 1,
        'img' => 1,
        'input' => 1,
        'keygen' => 1,
        'link' => 1,
        'meta' => 1,
        'param' => 1,
        'source' => 1,
        'track' => 1,
        'wbr' => 1,
    ];

    /**
     * @param $name
     * @param array $attributes
     * @return string
     */
    public static function startTag($name, $attributes = [])
    {
        if ($name === null || $name === false) {
            return '';
        }

        return "<$name" . static::renderTagAttributes($attributes) . '>';
    }

    /**
     * @param $name
     * @return string
     */
    public static function endTag($name)
    {
        if ($name === null || $name === false || isset(static::$voidElements[Str::lower($name)])) {
            return '';
        }

        return "</$name>";
    }

    /**
     * @param $name
     * @param string $content
     * @param array $attributes
     * @return string
     */
    public static function tag($name, $content = '', $attributes = [])
    {
        if ($name === null || $name === false) {
            return $content;
        }
        $html = static::startTag($name, $attributes);
        if (isset(static::$voidElements[Str::lower($name)])) {
            return $html;
        }

        $html .= $content;
        $html .= static::endTag($name);

        return $html;
    }

    /**
     * Wraps given content into conditional comments for IE, e.g., `lt IE 9`.
     * @param string $content raw HTML content.
     * @param string $condition condition string.
     * @return string generated HTML.
     */
    private static function wrapIntoCondition($content, $condition)
    {
        if (strpos($condition, '!IE') !== false) {
            return "<!--[if $condition]><!-->\n" . $content . "\n<!--<![endif]-->";
        }

        return "<!--[if $condition]>\n" . $content . "\n<![endif]-->";
    }
}