<?php

namespace modoufuture\utils\traits\html;

use modoufuture\utils\Arr;
use modoufuture\utils\Str;

trait Tags
{
    /**
     * doctype
     * @return string
     */
    public static function doctype()
    {
        return '<!doctype html>';
    }

    /**
     * 注释
     * @param $content
     * @return string
     */
    public static function annotation($content)
    {
        return '<!-- ' . str_replace(['<!--', '-->'], ['&lt!--', '--&gt'], $content) . ' -->';
    }

    /**
     * @param $content
     * @param array $attributes
     * @return string
     */
    public static function style($content, $attributes = [])
    {
        return static::tag('style', $content, $attributes);
    }

    /**
     * @param $content
     * @param array $attributes
     * @return string
     */
    public static function script($content, $attributes = [])
    {
        return static::tag('script', $content, $attributes);
    }

    /**
     * @param $url
     * @param array $attributes
     * @return string
     */
    public static function css($url, $attributes = [])
    {
        if (!isset($attributes['rel'])) {
            $attributes['rel'] = 'stylesheet';
        }
        $attributes['href'] = $url;

        if (isset($attributes['condition'])) {
            $condition = $attributes['condition'];
            unset($attributes['condition']);

            return static::wrapIntoCondition(static::tag('link', '', $attributes), $condition);
        } elseif (isset($attributes['noscript']) && $attributes['noscript'] === true) {
            unset($attributes['noscript']);

            return '<noscript>' . static::tag('link', '', $attributes) . '</noscript>';
        }

        return static::tag('link', '', $attributes);
    }

    /**
     * @param $url
     * @param array $attributes
     * @return string
     */
    public static function js($url, $attributes = [])
    {
        $attributes['src'] = $url;
        if (isset($attributes['condition'])) {
            $condition = $attributes['condition'];
            unset($attributes['condition']);

            return static::wrapIntoCondition(static::tag('script', '', $attributes), $condition);
        }

        return static::tag('script', '', $attributes);
    }

    /**
     * @param $text
     * @param null|string $url
     * @param array $attributes
     * @return mixed
     */
    public static function a($text, $url = null, $attributes = [])
    {
        if ($url !== null) {
            $attributes['href'] = $url;
        }

        return static::tag('a', $text, $attributes);
    }

    /**
     * @param $text
     * @param null|string $email
     * @param array $attributes
     * @return mixed
     */
    public static function mailto($text, $email = null, $attributes = [])
    {
        $attributes['href'] = 'mailto:' . ($email === null ? $text : $email);

        return static::tag('a', $text, $attributes);
    }

    /**
     * @param $text
     * @param null|string $tel
     * @param array $attributes
     * @return mixed
     */
    public static function tel($text, $tel = null, $attributes = [])
    {
        $attributes['href'] = 'tel:' . ($tel === null ? $text : $tel);

        return static::tag('a', $text, $attributes);
    }

    /**
     * @param $src
     * @param array $attributes
     * @return string
     */
    public static function img($src, $attributes = [])
    {
        $attributes['src'] = $src;

        if (isset($attributes['srcset']) && is_array($attributes['srcset'])) {
            $srcset = [];
            foreach ($attributes['srcset'] as $descriptor => $url) {
                $srcset[] = $url . ' ' . $descriptor;
            }
            $attributes['srcset'] = implode(',', $srcset);
        }

        if (!isset($attributes['alt'])) {
            $attributes['alt'] = '';
        }

        return static::tag('img', '', $attributes);
    }

    /**
     * @param $content
     * @param null|string $for
     * @param array $attributes
     * @return mixed
     */
    public static function label($content, $for = null, $attributes = [])
    {
        $attributes['for'] = $for;

        return static::tag('label', $content, $attributes);
    }

    /**
     * @param string $content
     * @param array $attributes
     * @return string
     */
    public static function button($content = 'Button', $attributes = [])
    {
        if (!isset($attributes['type'])) {
            $attributes['type'] = 'button';
        }

        return static::tag('button', $content, $attributes);
    }

    /**
     * @param string $content
     * @param array $attributes
     * @return string
     */
    public static function submit($content = 'Submit', $attributes = [])
    {
        $attributes['type'] = 'submit';

        return static::button($content, $attributes);
    }

    /**
     * @param string $content
     * @param array $attributes
     * @return string
     */
    public static function reset($content = 'Reset', $attributes = [])
    {
        $attributes['type'] = 'reset';

        return static::button($content, $attributes);
    }

    /**
     * @param $type
     * @param null|string $name
     * @param null|string $value
     * @param array $attributes
     * @return string
     */
    public static function input($type, $name = null, $value = null, $attributes = [])
    {
        if (!isset($attributes['type'])) {
            $attributes['type'] = $type;
        }
        $attributes['name'] = $name;
        $attributes['value'] = $value === null ? null : (string)$value;

        return static::tag('input', '', $attributes);
    }

    /**
     * @param string $value
     * @param array $attributes
     * @return string
     */
    public static function inputButton($value = 'Button', $attributes = [])
    {
        $attributes['type'] = 'button';
        $attributes['value'] = $value;

        return static::tag('input', '', $attributes);
    }

    /**
     * @param string $value
     * @param array $attributes
     * @return string
     */
    public static function inputSubmit($value = 'Submit', $attributes = [])
    {
        $attributes['type'] = 'submit';
        $attributes['value'] = $value;

        return static::tag('input', '', $attributes);
    }
    /**
     * @param string $value
     * @param array $attributes
     * @return string
     */
    public static function inputReset($value = 'Reset', $attributes = [])
    {
        $attributes['type'] = 'reset';
        $attributes['value'] = $value;

        return static::tag('input', '', $attributes);
    }
    /**
     * @param string $value
     * @param array $attributes
     * @return string
     */
    public static function inputHidden($name, $value = null, $attributes = [])
    {
        return static::input('hidden', $name, $value, $attributes);
    }
    /**
     * @param string $value
     * @param array $attributes
     * @return string
     */
    public static function inputPassword($name, $value = null, $attributes = [])
    {
        return static::input('password', $name, $value, $attributes);
    }
    /**
     * @param string $value
     * @param array $attributes
     * @return string
     */
    public static function inputText($name, $value = null, $attributes = [])
    {
        return static::input('text', $name, $value, $attributes);
    }
    /**
     * @param string $value
     * @param array $attributes
     * @return string
     */
    public static function inputFile($name, $value = null, $attributes = [])
    {
        return static::input('file', $name, $value, $attributes);
    }

    /**
     * @param $name
     * @param string $value
     * @param array $attributes
     * @return string
     */
    public static function textarea($name, $value = '', $attributes = [])
    {
        $attributes['name'] = $name;
        $doubleEncode = Arr::pull($attributes, 'doubleEncode');

        return static::tag('textarea', Str::encode($value, $doubleEncode), $attributes);
    }

    /**
     * @param $name
     * @param bool $checked
     * @param array $options
     * @return string
     */
    public static function radio($name, $checked = false, $options = [])
    {
        return static::booleanInput('radio', $name, $checked, $options);
    }

    /**
     * @param $name
     * @param bool $checked
     * @param array $options
     * @return string
     */
    public static function checkbox($name, $checked = false, $options = [])
    {
        return static::booleanInput('checkbox', $name, $checked, $options);
    }

    /**
     * @param $type
     * @param $name
     * @param bool $checked
     * @param array $options
     * @return string
     */
    protected static function booleanInput($type, $name, $checked = false, $options = [])
    {
        $options['checked'] = (bool) $checked;
        $value = array_key_exists('value', $options) ? $options['value'] : '1';
        if (isset($options['uncheck'])) {
            // add a hidden field so that if the checkbox is not selected, it still submits a value
            $hiddenOptions = [];
            if (isset($options['form'])) {
                $hiddenOptions['form'] = $options['form'];
            }
            $hidden = static::inputHidden($name, $options['uncheck'], $hiddenOptions);
            unset($options['uncheck']);
        } else {
            $hidden = '';
        }
        if (isset($options['label'])) {
            $label = $options['label'];
            $labelOptions = isset($options['labelOptions']) ? $options['labelOptions'] : [];
            unset($options['label'], $options['labelOptions']);
            $content = static::label(static::input($type, $name, $value, $options) . ' ' . $label, null, $labelOptions);
            return $hidden . $content;
        }

        return $hidden . static::input($type, $name, $value, $options);
    }

    /**
     * @param $items
     * @param array $attributes
     * @return string
     */
    public static function ul($items, $attributes = [])
    {
        $tag = Arr::pull($attributes, 'tag', 'ul');
        $encode = Arr::pull($attributes, 'encode', true);
        $formatter = Arr::pull($attributes, 'item');
        $separator = Arr::pull($attributes, 'separator',  "\n");
        $itemOptions = Arr::pull($attributes, 'itemOptions',  []);

        if (empty($items)) {
            return static::tag($tag, '', $attributes);
        }

        $results = [];
        foreach ($items as $index => $item) {
            if ($formatter !== null) {
                $results[] = call_user_func($formatter, $item, $index);
            } else {
                $results[] = static::tag('li', $encode ? Str::encode($item) : $item, $itemOptions);
            }
        }

        return static::tag(
            $tag,
            $separator . implode($separator, $results) . $separator,
            $attributes
        );
    }

    /**
     * @param $items
     * @param array $attributes
     * @return string
     */
    public static function ol($items, $attributes = [])
    {
        $attributes['tag'] = 'ol';
        return static::ul($items, $attributes);
    }

    /**
     * @param $name
     * @param null $selection
     * @param array $items
     * @param array $attributes
     * @return string
     */
    public static function listBox($name, $selection = null, $items = [], $attributes = [])
    {
        if (!array_key_exists('size', $attributes)) {
            $attributes['size'] = 4;
        }
        if (!empty($attributes['multiple']) && !empty($name) && substr_compare($name, '[]', -2, 2)) {
            $name .= '[]';
        }
        $attributes['name'] = $name;
        if (isset($attributes['unselect'])) {
            // add a hidden field so that if the list box has no option being selected, it still submits a value
            if (!empty($name) && substr_compare($name, '[]', -2, 2) === 0) {
                $name = substr($name, 0, -2);
            }
            $hidden = static::inputHidden($name, $attributes['unselect']);
            unset($attributes['unselect']);
        } else {
            $hidden = '';
        }
        $selectOptions = static::renderSelectOptions($selection, $items, $attributes);
        return $hidden . static::tag('select', "\n" . $selectOptions . "\n", $attributes);
    }

    /**
     * @param $name
     * @param null $selection
     * @param array $items
     * @param array $attributes
     * @return string
     */
    public static function dropDownList($name, $selection = null, $items = [], $attributes = [])
    {
        if (!empty($attributes['multiple'])) {
            return static::listBox($name, $selection, $items, $attributes);
        }
        $attributes['name'] = $name;
        unset($attributes['unselect']);
        $selectOptions = static::renderSelectOptions($selection, $items, $attributes);
        return static::tag('select', "\n" . $selectOptions . "\n", $attributes);
    }

    /**
     * @param $name
     * @param null $selection
     * @param array $items
     * @param array $attributes
     * @return string
     */
    public static function checkboxList($name, $selection = null, $items = [], $attributes = [])
    {
        if (substr($name, -2) !== '[]') {
            $name .= '[]';
        }
        if (Arr::isTraversable($selection)) {
            $selection = array_map('strval', (array)$selection);
        }
        $formatter = Arr::pull($attributes, 'item');
        $itemOptions = Arr::pull($attributes, 'itemOptions', []);
        $encode = Arr::pull($attributes, 'encode', true);
        $separator = Arr::pull($attributes, 'separator', "\n");
        $tag = Arr::pull($attributes, 'tag', 'div');

        $lines = [];
        $index = 0;
        foreach ($items as $value => $label) {
            $checked = $selection !== null &&
                (!Arr::isTraversable($selection) && !strcmp($value, $selection)
                    || Arr::isTraversable($selection) && Arr::isIn((string)$value, $selection));
            if ($formatter !== null) {
                $lines[] = call_user_func($formatter, $index, $label, $name, $checked, $value);
            } else {
                $lines[] = static::checkbox($name, $checked, array_merge($itemOptions, [
                    'value' => $value,
                    'label' => $encode ? Str::encode($label) : $label,
                ]));
            }
            $index++;
        }

        if (Arr::has($attributes, 'unselect')) {
            // add a hidden field so that if the list box has no option being selected, it still submits a value
            $name2 = substr($name, -2) === '[]' ? substr($name, 0, -2) : $name;
            $hidden = static::input($name2, Arr::pull($attributes, 'unselect'));
        } else {
            $hidden = '';
        }

        $visibleContent = implode($separator, $lines);

        if ($tag === false) {
            return $hidden . $visibleContent;
        }

        return $hidden . static::tag($tag, $visibleContent, $attributes);
    }

    /**
     * @param $name
     * @param null $selection
     * @param array $items
     * @param array $attributes
     * @return string
     */
    public static function radioList($name, $selection = null, $items = [], $attributes = [])
    {
        if (Arr::isTraversable($selection)) {
            $selection = array_map('strval', (array)$selection);
        }

        $formatter = Arr::pull($attributes,'item');
        $itemOptions = Arr::pull($attributes,'itemOptions', []);
        $encode = Arr::pull($attributes,'encode', true);
        $separator = Arr::pull($attributes,'separator', "\n");
        $tag = Arr::pull($attributes,'tag', 'div');
        // add a hidden field so that if the list box has no option being selected, it still submits a value
        $hidden = Arr::has($attributes, 'unselect') ? static::inputHidden($name, Arr::pull($attributes, 'unselect', '')) : '';

        $lines = [];
        $index = 0;
        foreach ($items as $value => $label) {
            $checked = $selection !== null &&
                (!Arr::isTraversable($selection) && !strcmp($value, $selection)
                    || Arr::isTraversable($selection) && Arr::isIn((string)$value, $selection));
            if ($formatter !== null) {
                $lines[] = call_user_func($formatter, $index, $label, $name, $checked, $value);
            } else {
                $lines[] = static::radio($name, $checked, array_merge($itemOptions, [
                    'value' => $value,
                    'label' => $encode ? Str::encode($label) : $label,
                ]));
            }
            $index++;
        }
        $visibleContent = implode($separator, $lines);

        if ($tag === false) {
            return $hidden . $visibleContent;
        }

        return $hidden . static::tag($tag, $visibleContent, $attributes);
    }

    /**
     * @param $selection
     * @param $items
     * @param array $tagOptions
     * @return string
     */
    public static function renderSelectOptions($selection, $items, &$tagOptions = [])
    {
        if (Arr::isTraversable($selection)) {
            $selection = array_map('strval', (array)$selection);
        }

        $lines = [];
        $encodeSpaces = Arr::pull($tagOptions, 'encodeSpaces', false);
        $encode = Arr::pull($tagOptions, 'encode', true);
        if (Arr::has($tagOptions, 'prompt')) {
            $promptOptions = ['value' => ''];
            $prompt = Arr::get($tagOptions, 'prompt', '');
            if (is_string($prompt)) {
                $promptText = $prompt;
            } else {
                $promptText = $prompt['text'];
                $promptOptions = array_merge($promptOptions, $prompt['options']);
            }
            $promptText = $encode ? Str::encode($promptText) : $promptText;
            if ($encodeSpaces) {
                $promptText = str_replace(' ', '&nbsp;', $promptText);
            }
            $lines[] = static::tag('option', $promptText, $promptOptions);
        }

        $options = Arr::has($tagOptions, 'options') ? Arr::get($tagOptions, 'options') : [];
        $groups = Arr::has($tagOptions, 'groups') ? Arr::get($tagOptions, 'groups') : [];
        Arr::remove($tagOptions, ['prompt', 'options', 'groups']);
        $options['encodeSpaces'] = isset($options['encodeSpaces']) ? $options['encodeSpaces'] : $encodeSpaces;
        $options['encode'] = isset($options['encode']) ? $options['encode'] : $encode;

        foreach($items as $key => $value) {
            if (is_array($value)) {
                $groupAttrs = isset($groups[$key]) ? $groups[$key] : [];
                if (!isset($groupAttrs['label'])) {
                    $groupAttrs['label'] = $key;
                }
                $attrs = ['options'=>$options, 'groups'=>$groups, 'encodeSpaces'=>$encodeSpaces, 'encode'=>$encode];
                $content = static::renderSelectOptions($selection, $value, $attrs);
                $lines[] = static::tag('optgroup', "\n".$content."\n", $groupAttrs);
            } else {
                $attrs = isset($options[$key]) ? $options[$key] : [];
                $attrs['value'] = (string)$key;
                if (!array_key_exists('selected', $attrs)) {
                    $attrs['selected'] = $selection !== null &&
                        (!Arr::isTraversable($selection) && !strcmp($key, $selection)
                            || Arr::isTraversable($selection) && Arr::isIn((string)$key, $selection));
                }
                $text = $encode ? Str::encode($value) : $value;
                if ($encodeSpaces) {
                    $text = str_replace(' ', '&nbsp;', $text);
                }
                $lines[] = static::tag('option', $text, $attrs);
            }
        }

        return implode("\n", $lines);
    }
}