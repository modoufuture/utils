<?php

namespace modoufuture\utils\traits\arr;

use modoufuture\utils\Json;
use modoufuture\utils\Xml;

trait Items
{
    /**
     * @param array $array
     * @param string $key
     * @param mixed $value
     * @return mixed
     */
    public static function add(&$array, $key, $value)
    {
        if (is_null(static::get($array, $key))) {
            static::set($array, $key, $value);
        }

        return $array;
    }

    /**
     * @param mixed $array
     * @param string|null|\Closure|mixed $key
     * @param null|mixed $default
     * @return null|mixed
     */
    public static function get($array, $key, $default = null)
    {
        if (is_null($key)) {
            return $array;
        } elseif ($key instanceof \Closure) {
            return $key($array, $default);
        }

        if (isset($array[$key])) {
            return $array[$key];
        }
        $keys = static::splitKey($key);
        foreach($keys as $key) {
            if (!static::isArray($array) || !array_key_exists($key, $array)) {
                return $default;
            }
            $array = $array[$key];
        }

        return $array;
    }

    /**
     * @param array $array
     * @param string $key
     * @param mixed $value
     * @return mixed
     */
    public static function set(&$array, $key, $value)
    {
        if (is_null($key)) {
            return $array = $value;
        }
        $keys = static::splitKey($key);
        $tmp = $array;
        while (count($keys) > 1) {
            $key = array_shift($keys);
            if (!$tmp[$key] || !is_array($tmp[$key])) {
                $tmp[$key] = [];
            }
            $tmp = &$tmp[$key];
        }
        $tmp[array_shift($keys)] = $value;
        $array = $tmp;
        unset($tmp);

        return $array;
    }

    /**
     * @param mixed $array
     * @param string $key
     * @return bool
     */
    public static function has($array, $key)
    {
        return !is_null(static::get($array, $key, null));
    }

    /**
     * @param array|\Traversable $array
     * @return array
     */
    public static function flatten($array)
    {
        $result = [];
        array_walk_recursive(
            $array,
            function($value) use(&$result){
                $result[] = $value;
            }
        );

        return $result;
    }

    /**
     * @param array $array
     * @param string $prepend
     * @return array
     */
    public static function flattenWithDot($array, $prepend = '')
    {
        if (!static::canSplit()) {
            return static::flatten($array);
        }
        $result = [];
        foreach($array as $k=>$v) {
            if (is_array($v)) {
                $result = array_merge($result, static::flattenWithDot($v, $prepend.$k.static::$delimit));
            } else {
                $result[$prepend.$k] = $v;
            }
        }

        return $result;
    }

    /**
     * @param $arr1
     * @param $arr2
     * @return array|mixed
     */
    public static function merge($arr1, $arr2)
    {
        $args = func_get_args();
        $res = array_shift($args);

        while (!empty($args)) {
            foreach(array_shift($args) as $k=>$v) {
                if (is_int($k)) {
                    if (array_key_exists($k, $res)) {
                        $res[] = $v;
                    } else {
                        $res[$k] = $v;
                    }
                } elseif (is_array($v) && isset($res[$k]) && is_array($res[$k])) {
                    $res[$k] = static::merge($res[$k], $v);
                } else {
                    $res[$k] = $v;
                }
            }
        }

        return $res;
    }

    /**
     * @param array $array
     * @param string|array $keys
     * @return mixed
     */
    public static function remove(&$array, $keys)
    {
        $ori = &$array;
        foreach ((array)$keys as $key) {
            $ks = static::splitKey($key);
            $count = count($ks);
            while ($count > 1) {
                $k = array_shift($ks);
                if (isset($array[$k]) && is_array($array[$k])) {
                    $array = &$array[$k];
                }
                $count--;
            }
            unset($array[array_shift($ks)]);
            $array = $ori;
        }
        unset($ori);

        return $array;
    }

    /**
     * @param array $array
     * @param string $key
     * @param null|mixed $default
     * @return mixed|null
     */
    public static function pull(&$array, $key, $default = null)
    {
        $result = static::get($array, $key, $default);
        static::remove($array, $key);

        return $result;
    }

    /**
     * @param array $array
     * @param int $options
     * @return string
     * @throws \Exception
     */
    public static function toJson($array, $options = JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE)
    {
        return Json::encode($array, $options);
    }

    /**
     * @param array $array
     * @param array $options
     * @return string
     */
    public static function toXml($array, $options = [])
    {
        return Xml::encode($array, $options);
    }

    /**
     * @param $array
     * @param bool $pretty_print
     * @param bool $short
     * @param string $output
     * @param int $level
     * @return string
     */
    public static function toString($array, $pretty_print = false, $short = true, &$output='', $level=1)
    {
        $lt = $short ? '[ ' : 'array( ';
        $rt = $short ? ' ]' : ' )';
        if ($pretty_print) {
            $tab = str_repeat("    ", $level);
            $tab_end = str_repeat('    ', max($level-1, 0));
            $tab_parent = "\n".str_repeat('    ', max($level-2, 0));
        } else {
            $tab = $tab_end = $tab_parent = '';
        }
        $output.= $lt.($pretty_print?"\n":"");
        foreach($array as $key => $value) {
            $output .= $tab.'"'.str_replace('"', '\"', $key).'" => ';
            if (static::isTraversable($value)) {
                if (!empty($value)) {
                    static::toString($value, $pretty_print, $short, $output, $level+1);
                    $output .= ', '.$tab_parent;
                } else {
                    $output .= $lt.$rt;
                    $output .= ', '.$tab_parent;
                }
            } else {
                $output .= var_export($value, true).', '.$tab_parent;
            }
        }
        $output .= $tab_end.$rt;
        return $output;
    }

    /**
     * @param array $array
     * @param int $mode
     * @return int
     */
    public static function count($array, $mode = self::COUNT_NORMAL)
    {
        if ($mode === self::COUNT_RECURSIVE_WITHOUT_SELF) {
            $array = static::flatten($array);

            $mode = self::COUNT_NORMAL;
        }

        return count($array, $mode);
    }

    /**
     * @param array|\Traversable $array
     * @param string $column
     * @param bool $keepKey
     * @return array
     */
    public static function column($array, $column, $keepKey = true)
    {
        $result = [];
        if ($keepKey) {
            foreach ($array as $k => $value) {
                $result[$k] = static::get($value, $column);
            }
        } else {
            foreach ($array as $value) {
                $result[] = static::get($value, $column);
            }
        }

        return $result;
    }

    /**
     * @param array $array
     * @param string|\Closure $from
     * @param string|\Closure $to
     * @param string|\Closure $group
     * @example
     * ```php
     * $array = [
     *     ['id' => '123', 'name' => 'aaa', 'class' => 'x'],
     *     ['id' => '124', 'name' => 'bbb', 'class' => 'x'],
     *     ['id' => '345', 'name' => 'ccc', 'class' => 'y'],
     * ];
     *
     * $result = Arr::columns($array, 'id', 'name');
     * // the result is:
     * // [
     * //     '123' => 'aaa',
     * //     '124' => 'bbb',
     * //     '345' => 'ccc',
     * // ]
     *
     * $result = Arr::columns($array, 'id', 'name', 'class');
     * // the result is:
     * // [
     * //     'x' => [
     * //         '123' => 'aaa',
     * //         '124' => 'bbb',
     * //     ],
     * //     'y' => [
     * //         '345' => 'ccc',
     * //     ],
     * // ]
     * ```
     * @return array
     */
    public static function columns($array, $key, $column, $group = null)
    {
        $result = [];
        foreach ($array as $value) {
            $key = static::get($value, $key);
            $value = static::get($value, $column);
            if ($group !== null) {
                $result[static::get($value, $group)][$key] = $value;
            } else {
                $result[$key] = $value;
            }
        }

        return $result;
    }

    /**
     * @param $array
     * @param $key
     * @param int $direction
     * @param int $sortFlag
     */
    public static function multiSort(&$array, $key, $direction = SORT_ASC, $sortFlag = SORT_REGULAR)
    {
        $keys = is_array($key) ? $key : [$key];
        if (empty($keys) || empty($array)) {
            return;
        }
        $n = count($keys);
        if (is_scalar($direction)) {
            $direction = array_fill(0, $n, $direction);
        } elseif (count($direction) !== $n) {
            throw new \InvalidArgumentException('The length of $direction parameter must be the same as that of $keys.');
        }
        if (is_scalar($sortFlag)) {
            $sortFlag = array_fill(0, $n, $sortFlag);
        } elseif (count($sortFlag) !== $n) {
            throw new \InvalidArgumentException('The length of $sortFlag parameter must be the same as that of $keys.');
        }
        $args = [];
        foreach ($keys as $i => $key) {
            $flag = $sortFlag[$i];
            $args[] = static::column($array, $key);
            $args[] = $direction[$i];
            $args[] = $flag;
        }

        // This fix is used for cases when main sorting specified by columns has equal values
        // Without it it will lead to Fatal Error: Nesting level too deep - recursive dependency?
        $args[] = range(1, count($array));
        $args[] = SORT_ASC;
        $args[] = SORT_NUMERIC;

        $args[] = &$array;
        call_user_func_array('array_multisort', $args);
    }

    /**
     * @param $data
     * @param bool $valuesOnly
     * @param string $charset
     * @return array
     */
    public static function htmlEncode($data, $valuesOnly = true, $charset = 'UTF-8')
    {
        $d = [];
        foreach ($data as $key => $value) {
            if (!$valuesOnly && is_string($key)) {
                $key = htmlspecialchars($key, ENT_QUOTES | ENT_SUBSTITUTE, $charset);
            }
            if (is_string($value)) {
                $d[$key] = htmlspecialchars($value, ENT_QUOTES | ENT_SUBSTITUTE, $charset);
            } elseif (is_array($value)) {
                $d[$key] = static::htmlEncode($value, $valuesOnly, $charset);
            } else {
                $d[$key] = $value;
            }
        }

        return $d;
    }

    /**
     * @param $data
     * @param bool $valuesOnly
     * @return array
     */
    public static function htmlDecode($data, $valuesOnly = true)
    {
        $d = [];
        foreach ($data as $key => $value) {
            if (!$valuesOnly && is_string($key)) {
                $key = htmlspecialchars_decode($key, ENT_QUOTES);
            }
            if (is_string($value)) {
                $d[$key] = htmlspecialchars_decode($value, ENT_QUOTES);
            } elseif (is_array($value)) {
                $d[$key] = static::htmlDecode($value);
            } else {
                $d[$key] = $value;
            }
        }

        return $d;
    }

    /**
     * Filters array according to rules specified.
     *
     * For example:
     *
     * ```php
     * $array = [
     *     'A' => [1, 2],
     *     'B' => [
     *         'C' => 1,
     *         'D' => 2,
     *     ],
     *     'E' => 1,
     * ];
     *
     * $result = \yii\helpers\ArrayHelper::filter($array, ['A']);
     * // $result will be:
     * // [
     * //     'A' => [1, 2],
     * // ]
     *
     * $result = \yii\helpers\ArrayHelper::filter($array, ['A', 'B.C']);
     * // $result will be:
     * // [
     * //     'A' => [1, 2],
     * //     'B' => ['C' => 1],
     * // ]
     *
     * $result = \yii\helpers\ArrayHelper::filter($array, ['B', '!B.C']);
     * // $result will be:
     * // [
     * //     'B' => ['D' => 2],
     * // ]
     * ```
     *
     * @param array $array Source array
     * @param array $filters Rules that define array keys which should be left or removed from results.
     * Each rule is:
     * - `var` - `$array['var']` will be left in result.
     * - `var.key` = only `$array['var']['key'] will be left in result.
     * - `!var.key` = `$array['var']['key'] will be removed from result.
     * @return array Filtered array
     */
    public static function filter($array, $filters)
    {
        $result = [];
        $forbiddenVars = [];

        foreach ($filters as $var) {
            $keys = static::splitKey($var);
            $globalKey = $keys[0];
            $localKey = isset($keys[1]) ? $keys[1] : null;

            if ($globalKey[0] === '!') {
                $forbiddenVars[] = [
                    substr($globalKey, 1),
                    $localKey,
                ];
                continue;
            }

            if (!array_key_exists($globalKey, $array)) {
                continue;
            }
            if ($localKey === null) {
                $result[$globalKey] = $array[$globalKey];
                continue;
            }
            if (!isset($array[$globalKey][$localKey])) {
                continue;
            }
            if (!array_key_exists($globalKey, $result)) {
                $result[$globalKey] = [];
            }
            $result[$globalKey][$localKey] = $array[$globalKey][$localKey];
        }

        foreach ($forbiddenVars as $var) {
            list($globalKey, $localKey) = $var;
            if (array_key_exists($globalKey, $result)) {
                unset($result[$globalKey][$localKey]);
            }
        }

        return $result;
    }
}