<?php

namespace modoufuture\utils\traits\arr;

trait Split
{
    protected static $delimit = '.';

    /**
     * @return bool
     */
    public static function canSplit()
    {
        return static::$delimit !== null && static::$delimit !== false && static::$delimit !== '';
    }

    /**
     * @param string|mixed $delimit
     */
    public static function setDelimit($delimit = '.')
    {
        static::$delimit = $delimit;
    }

    /**
     * @param mixed $key
     * @return array|mixed
     */
    public static function splitKey($key)
    {
        if (static::canSplit()) {
            return explode(static::$delimit, $key);
        }

        return [$key];
    }
}