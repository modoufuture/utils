<?php

namespace modoufuture\utils\traits\arr;

trait Is
{
    /**
     * @param mixed $arr
     *
     * @return bool
     */
    public static function isTraversable($arr)
    {
        return is_array($arr) || $arr instanceof \Traversable;
    }

    /**
     * @param mixed $needle
     * @param array|\Traversable $haystack
     * @param bool $strict
     * @return bool
     */
    public static function isIn($needle, $haystack, $strict = false)
    {
        if ($haystack instanceof \Traversable) {
            foreach ($haystack as $value) {
                if ($needle == $value && (!$strict || $needle === $value)) {
                    return true;
                }
            }
        } elseif (is_array($haystack)) {
            return in_array($needle, $haystack, $strict);
        } else {
            throw new \InvalidArgumentException('Argument $haystack must be an array or implement Traversable');
        }

        return false;
    }

    /**
     * @param $needles
     * @param $haystack
     * @param bool $strict
     * @return bool
     */
    public static function isSubset($needles, $haystack, $strict = false)
    {
        if (is_array($needles) || $needles instanceof \Traversable) {
            foreach ($needles as $needle) {
                if (!static::isIn($needle, $haystack, $strict)) {
                    return false;
                }
            }

            return true;
        }

        throw new \InvalidArgumentException('Argument $needles must be an array or implement Traversable');
    }
    /**
     * @param mixed $value
     * @return bool
     */
    public static function isArray($value)
    {
        return is_array($value);
    }

    /**
     * 检查是否是索引数组
     * @param $array
     * @param bool $consecutive
     * @return bool
     */
    public static function isIndexed($array, $consecutive = false)
    {
        if (!is_array($array)) {
            return false;
        }

        if (empty($array)) {
            return true;
        }

        if ($consecutive) {
            return array_keys($array) === range(0, count($array) - 1);
        }

        foreach ($array as $key => $value) {
            if (!is_int($key)) {
                return false;
            }
        }

        return true;
    }

    /**
     * 检查是否是关联数组
     *
     * @param $array
     * @param bool $allStrings
     * @return bool
     */
    public static function isAssociative($array, $allStrings = true)
    {
        if (!is_array($array) || empty($array)) {
            return false;
        }

        if ($allStrings) {
            foreach ($array as $key => $value) {
                if (!is_string($key)) {
                    return false;
                }
            }

            return true;
        }

        foreach ($array as $key => $value) {
            if (is_string($key)) {
                return true;
            }
        }

        return false;
    }
}